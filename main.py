#Import Flask is to use Flask API
#Import render_template is to render a template HTML in your python file
#Import redirect for redirecting program to use onebrick sandbox api
#Import request in flask library to get json value from post redirect in demo sandbox
from flask import Flask, render_template,redirect,request
#Import Request is to access a link to retrieve the json value
#Import urlopen is to access the url object that is retrieved from  Request
#Import base64 is to encode and decode
from urllib.request import Request, urlopen,base64
#Import CORS to enable all of HTTP headers mechanism
from flask_cors import CORS, cross_origin
#read json format
import json
#Import random and string for generating session
import random,string
#Import datetime to get current date
from datetime import datetime

#read user Keys
with open('key.txt') as inputfile:
    list_key = []
    for line in inputfile:
        x = line.replace(' ','').replace('\n','').split('=')
        list_key.append(x[1])
clientID = list_key[0]
clientSecret = list_key[1]
if (clientID == '' or clientSecret == ''):
    print("Please insert your clientID and clientSecret in key.txt and restart the application...")
    exit()

#Flask app configuration
app = Flask(__name__)
#CORS configuration for any header to be accessed in HTTP
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


#dictionary dict to store the contents of transaction details
dictionary= {
    "session":"",
    "token" :"",
    "date":[],
    "description":[],
    "status":[],
    "amount":[],
}

#user_account dict to store the user's accounts detail in sandbox.onebrick.io/account/list
user_account= {
    "accountHolder":[],
    "accountNumber" :[],
    "accountId":[],
    "balances":{
        "available":"",
        "current":"",
    }
}

#user_detail dict to store the specific user's account detail
user_detail= {
    "accountHolder":"",
    "accountNumber" :"",
    "accountId":"",
    "balances":{
        "available":"",
        "current":"",
    }
}

#boolean if user uses Jenius Bank
boolean = {
    "isJenius" : False
 }

#jenius dictionary to store jenius transaction and account details
jenius = {
    "jeniusTransactionDetail":[],
    "jeniusAccountDetail":[]
}
#base link
base_link = "https://sandbox.onebrick.io/v1/"

#first function to run when python file is executed
@app.route("/")
def main():
    return render_template('Start.html')

#trigger functionn when form is posted in 'Start.html'
@app.route("/goto",methods=['POST'])
def goto():
    #call function authentication() to get the JWT-public-access-token
    token = authentication()
    #url to access onebrick.io API
    url = base_link+"index?"+"accessToken="+token+"&redirect_url="+"http://localhost:5000/result"
    return redirect(url)

#trigger the function when moneysave demo do method post
@app.route("/result",methods=['POST'])
def postResult():
    #reset jenius dictionary
    jenius['jeniusTransactionDetail'] = []
    #collect the user-access-token that is available once user sign in the bank account
    data = request.json
    #temp to store the value
    temp = data["accessToken"]
    #condition for data retrieve from POST response is more than 2, then the user is using Jenius
    if(len(data) > 2):
        #condition for transaction not empty
        if(data['transactions'] is not None):
            #set boolean isJenius to true
            boolean['isJenius'] = True
            #populate jenius transaction to dictionary
            temp2 = json.loads(data['transactions'])
            jenius['jeniusTransactionDetail'] = temp2.copy()
        #condition for accounts not empty
        if(data['accounts'] is not None):
            #populate jenius account detail to dictionary
            temp3 = json.loads(data['accounts'])
            jenius['jeniusAccountDetail'] = temp3.copy()
    #randomize a sessionID
    session = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(20))
    #condition to check when accessToken is not found, then we will save user-access-token to ''
    if(temp == ''):
        #store the user-access-token into our dictionary
        dictionary["token"] = ''
        return "http://localhost:5000/result?sessionId="+session
    #condition when accessToken is found
    else:
        #store the user-access-token into our dictionary
        dictionary["token"] = temp
        #store sessionID to dictionary
        dictionary["session"] = session
        return "http://localhost:5000/result?sessionId="+session

#trigger function when moneysave demo do method get
@app.route("/result",methods=['GET'])
def getResult():
    #when user-access-token is not found, we return error html
    if(dictionary["token"]==''):
        return render_template('Error.html')
    #when user-access-token is found, we return result/transaction page
    else:
        #condition to check if user is using Jenius Bank
        if(boolean['isJenius'] == False):
            return render_template('Result.html',dictionary=getTransaction(), userdetail = getUserDetail(), date=getDate())
        else:
            return render_template('Result.html',dictionary=getTransactionJenius(), userdetail = getUserDetailJenius() ,date=getDate())

#function to get the JWT-public-access-token
def authentication():
    account = clientID +":" +clientSecret
    #perform base 64 encoding for basic authentication
    authorize = "Basic " + base64.b64encode(account.encode()).decode()
    #initialize header to access the url
    header = {
        'Content-type':'application/json',
        'Authorization':authorize}
    #retrieve the json value from the link
    request = Request(base_link+'auth/token', headers=header)
    response = urlopen(request).read().decode('utf-8') 
    data = json.loads(response)['data']
    return data['access_token'] 

#function to get all the transaction in a user's account
def getTransaction():
    #clear the dictionary value so that everytime page is refreshed no information is duplicated
    dictionary['date'] =[]
    dictionary['description'] = []
    dictionary['status'] = []
    dictionary['amount']=[]
    userToken = dictionary['token']
    #initialize header to access the url
    header = {
        'Content-type':'application/json',
        'Authorization':'Bearer '+userToken}
    #retrieve the json value from the link
    #change the date range to your choice
    request = Request(base_link+'transaction/list?from=2021-01-01&to=2021-02-17', headers=header)
    response = urlopen(request).read().decode('utf-8')
    data = json.loads(response)['data']
    #store the information of the transaction's date, description, status and amount into our dictionary
    for i in range (len(data)):
        dictionary['date'].append(data[i]['date'])
        dictionary['description'].append(data[i]['description'])
        dictionary['status'].append(data[i]['status'])
        dictionary['amount'].append("{:,}".format(float(data[i]['amount'])))
    return dictionary

def getTransactionJenius():
    #reset dictionary value
    dictionary['date'] =[]
    dictionary['description'] = []
    dictionary['status'] = []
    dictionary['amount']=[]
    #populate dictionary for transaction from response in POST method of Brick API
    for i in range(len(jenius['jeniusTransactionDetail'])):
        dictionary['date'].append(jenius['jeniusTransactionDetail'][i]['date'])
        dictionary['description'].append(jenius['jeniusTransactionDetail'][i]['description'])
        dictionary['status'].append(jenius['jeniusTransactionDetail'][i]['status'])
        dictionary['amount'].append(jenius['jeniusTransactionDetail'][i]['amount'])
    return dictionary

#function to retrieve all user's account in 1 user-access-token (in the same bank)
def getUserAccounts():
    #userToken is the user-access-token that we get from dictionary
    userToken = dictionary['token']
    #initialize header to access the url
    header = {
        'Content-type':'application/json',
        'Authorization':'Bearer '+userToken}
    #retrieve the json value from the link
    request = Request(base_link+'account/list', headers=header)
    response = urlopen(request).read().decode('utf-8')
    data = json.loads(response)['data']
    #condition to store default value when the data in sandbox.onebrick.io/v1/account/list is empty (specifically for mockbank account)
    if(len(data) == 0):
        user_account['accountHolder'].append('Johndoe')
        user_account['accountNumber'].append('9870675789')
        user_account['accountId'].append('qwerty//++--==')
        user_account['balances']['available'] = "100,987"
        user_account['balances']['current'] = "100,987"
    #condition when the value is not empty
    else:
        for i in range (len(data)):
            #store all the information to user account information
            user_account['accountHolder'].append(data[i]['accountHolder'])
            user_account['accountNumber'].append(data[i]['accountNumber'])
            user_account['accountId'].append(data[i]['accountId'])
            user_account['balances']['available'] = "{:,}".format(data[i]['balances']['available'])
            user_account['balances']['current'] = "{:,}".format(data[i]['balances']['current'])
    return user_account

#function to  get the user account detail from 1 account ID
def getUserDetail():
    #userToken to store the user-access-token
    userToken = dictionary['token']
    #accountId is to get which specific account that is being accessed
    accountId = getUserAccounts()['accountId'][0]
    #initialize header to access the url
    header = {
        'Content-type':'application/json',
        'Authorization':'Bearer '+userToken}
    #retrieve the json value from the link
    request = Request(base_link+'account/detail?accountId='+accountId, headers=header)
    response = urlopen(request).read().decode('utf-8')
    data = json.loads(response)['data']
    #store all the data from the json into user_detail dictionary
    user_detail['accountHolder']=data['accountHolder']
    user_detail['accountNumber']=data['accountNumber']
    user_detail['accountId']=data['accountId']
    user_detail['balances']['available'] = "{:,}".format(data['balances']['available'])
    user_detail['balances']['current'] = "{:,}".format(data['balances']['current'])
    return user_detail

def getUserDetailJenius():
    #reset user_detail value
    user_detail['accountHolder'] =""
    user_detail['accountNumber'] = ""
    user_detail['acccountId'] = ""
    user_detail['balances']['available'] =""
    user_detail['balances']['current'] =""
    #populate dictionary for transaction from response in POST method of Brick API
    for i in range(len(jenius['jeniusAccountDetail'])):
        user_detail['accountHolder']= jenius['jeniusAccountDetail'][i]['accountHolder']
        user_detail['accountNumber']= jenius['jeniusAccountDetail'][i]['accountNumber']
        user_detail['acccountId']= jenius['jeniusAccountDetail'][i]['accountId']
        user_detail['balances']['available']="{:,}".format(jenius['jeniusAccountDetail'][i]['balances']['available'])
        user_detail['balances']['current']="{:,}".format(jenius['jeniusAccountDetail'][i]['balances']['current'])
    return user_detail

#function to get the current date when the program is running
def getDate():
    date= datetime.today().strftime('%d %B %Y')
    return date

#debugging purpose
if __name__ == "__main__":
    app.run()